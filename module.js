module.exports = function(config) {
    return new VirtualdubEncoder(config);
};

var path = require('path');
var childProcess = require('child_process');
var Utils = require(path.join(__dirname, "Utils.js"));
var configDefaults = require(path.join(__dirname, "defaults.js"));

function VirtualdubEncoder(config) {
    this.setConfig(config);
}

VirtualdubEncoder.prototype.setConfig = function(config) {
    this.config = Utils.objectMerge(configDefaults, config);
};

VirtualdubEncoder.prototype.getConfig = function() {
    return this.config;
};

VirtualdubEncoder.prototype.start = function() {
    var self = this;

    var cmd = self.virtualdubCmd(self.config);
    if(self.config.debug)
        console.log("VIRTUALDUB PARAMS: " + cmd);
    var virtualdubProcess = childProcess.spawn(self.config.bins.virtualdub, cmd.split(' '));

    virtualdubProcess.on('exit', function(exitCode) {
        if(typeof self.config.complete === "function") {
            if(exitCode == 0)
                self.config.complete(null, true);
            else
                self.config.complete(exitCode, null);
        }

        // stop() function was called
        if(typeof self.stopCallback === "function") {
            self.stopCallback(null, true);
            delete self.stopCallback;
        }
    });

    self.virtualdubProcess = virtualdubProcess;
};

VirtualdubEncoder.prototype.stop = function(callback) {
    if(typeof callback === "function")
        this.stopCallback = callback;
    this.virtualdubProcess.kill();
};

VirtualdubEncoder.prototype.probe = function(callback) {
    childProcess.exec(this.config.bins.ffprobe + " -v quiet -print_format json -show_format -show_streams " + this.config.input, function(error, stdout, stderr) {
        if(error)
            callback(error, null);
        else {
            try {
                var mediaInfo = JSON.parse(stdout);
                callback(null, mediaInfo);
            }
            catch(error) {
                callback("parse_error", null);
            }
        }
    });
};

VirtualdubEncoder.prototype.virtualdubCmd = function(config) {
    var cmd = "/c";
    if(typeof config.script === "string")
        cmd += " /s " + config.script;

    if(typeof config.input === "string")
        cmd += " /p " + config.input;
    if(typeof config.output === "string")
        cmd += " " + config.output;

    if(typeof config.params === "string")
        cmd += " "+ config.params;
    else if(typeof config.params === "object") {
        Utils.objectForEach(config.params, function(param, key) {
            if(Array.isArray(param)) {
                param.forEach(function(value) {
                    cmd += " /" + key + " " + value;
                });
            }
            else
                cmd += " -" + key + " " + param;
        });
    }
    cmd += " /r /x";

    return cmd;
};