var config = {};

var path = require('path');

config.bins = {
    virtualdub: path.join(__dirname, "bin", "virtualdub", "VirtualDub.exe"),
    ffprobe: path.join(__dirname, "bin", "ffprobe.exe")
};

config.script = path.join(__dirname, "virtualdub_scripts", "1920x1080.vcf");

module.exports = config;