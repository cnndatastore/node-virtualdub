From raythe0n's origional plugin, modified by tateu, later modified by dloneranger

Requires Virtualdub version 1.10.xx

V1.8.2.7 [4 April 2015]
Add check video stream at 1/3, 1/2 and 2/3 for height, width and aspect ratio
> then use the biggest available as fix for streams that have a smaller sized intro

V1.8.2.6 [26 March 2015]
Update ffdlls to 2.6.1
Fix make prescan option work again after disabling it for testing in previous version

V1.8.2.5 [8 March 2015]
Update ffdlls to 2.6.0
Add checkbox to enable/disable priority - with it enabled, virtualdub is giving us .avs files even though we don't want them
Change version number so it's higher than a very old version that was released

V0.8.2.4 [25 February 2015]
Added set plugin priority in the extended options dialog and file information dialog
Added plugin is selectable from audio menu 'from another file'
Fix better handling of truncated audio stream
Fix seek recursion endless loop
Modify fallback of reading options from HKLM registry in case an installer wants to set any options on install
Fix bug in seeking loop (accidental = instead of ==)

V0.8.2.3 [14 February 2015]
Update ffdlls to 2.5.4
Fix small bug in offset audio when dropped frames are at the beginning
Modify ensure width/height are mod2 when aspect scaling is enabled
Modify use lanczos scaling when aspect ratio is enabled

V0.8.2.2 [31 January 2015]
Modified use longest duration of audio when stream and format lengths are different
Modify pad end of audio packet when at the end of an audio stream
Fix reading very first audio packet when seeking
Fix honouring the sync video/audio option when turned off
Modify try reducing some seeks in the audio
Modify add extra codec info to file information dialog
Fix video stream may change height/width during decoding, caused green bars at bottom or right
Fix decode error in sws_scale that could crash on certain height/widths when playing in virtualdub
Modify file information dialog to add some more info
Modify audio delay when first frames are dropped 

V0.8.2.1 [24 January 2015]
Workaround seeking in wmv files could be way off - ask ffdlls for frame 2000 and actually get 2500..sigh...
Fix setting the first audio stream to the best choice relating to the video when there are multiple audio streams
Modify don't autosync audio that's more than 15 minutes out of sync
> fix for some .ts files where audio streams are marked as starting a log way off from the video

V0.8.2.0 [20 January 2015]
Update ffdlls to 2.5.3
Fix bug where seeking would get stuck in a loop
> silly mistake, a - should have been a + so instead of moving back trying to find the frame it moved forwards
> couldn't find the frame and would try again, still moving forwards..... oops
Fix once the above bug was fixed, retrying could be improved to it quits faster on undecodable video frames
> with the bug above, the code for undecodable frames was called for valid frames
Fix with both of those fixed, some nasty peekmessage code could be stripped out
> it was only there to keep the buttons working in virtualdub when the plugin was stuck in a loop
> so you could move to a different frame and get it out of the loop
> it was a very nasty hack!!
>and all because of a minus sign.....
Fix files with extra video streams of 1 frame thumbnail images could confuse the plugin
Change files with no keyframe index information are scanned when opened to let the ffmpeg dlls build one
>this is an option in the plugins preferences (on by default)
>may cause a delay on opening a file, but quicker seeking when open
Fix trying to cut down on possibly unneeded seeks to try and make playing a video in virtualdub smoother (less stuttering on audio)
Fix sometimes a missing frame in the video would manage to skip the code that reports an error to virtualdub
>then virtualdub would try again, looping until it was killed
Fix bug not clearing buffers after a seek sometimes

V0.8.1.9 [29 December 2014]
Update ffdlls to 2.5.2
Fix very old audio positioning bug
Fix filling mono empty data with correct value
Fix try to correct sync of audio at the start when it's been offset
Fix crash at end of video
Fix repeat last frame at end of video where b-frames cause a shift at the start
Fix strange error where you'd get 90000fps for the source file (thanks shek for finding it)
Fix error that would slow decoding if frame rates were being altered eg by IVTC
Fix seek error in some formats where seeking on the audio failed
Fix seek to first frame for some formats
Remove .avs from default file extensions, avisynth already needs to be installed and virtualdub already handles .avs better
Fix setting first starting frame to only skip leading undecodable b-frames as there are vids with no I-frames (just P&B)
Improve seeking in audio with some formats

v0.8.1.8 [16 December 2014]
Update ffdlls to 2.5.1
Fix first starting frame where there are leading b-frames

v0.8.1.7 [30 August 2014]
Update ffdlls to 2.3.2
- delete the old ones, these have new numbering
Added option(on by default) to sync the audio timestamp start to the video one
- fixes some out of sync problems where the audio is off by the same amount all the way through the video

v0.8.1.6 [17 July 2014]
Update ffdlls to 2.3.0
FFDlls now need swresample-0.dll also
swresample-0.dll added

v0.8.1.5 [26 May 2014]
Update ffdlls to 2.2.2

v0.8.1.4 [20 Apr 2014]
Added error checking for missing ffmpeg dlls
Larger maximum index size for seeking
Timeout for decoding video/audio frames of 2 minutes so job's don't get stuck on queue
(->not perfect, virtualdub can still get in a seemingly endless loop on some errors)
Reporting decoding errors back to virtualdub on discontinuity
Error handling option for allowing an occasional missing frame(s)
More accurate discontinuity handling, will backtrack further through the file before giving up on a missing frame
Better fps detection
Some better handling of decoding errors

v0.8.1.3 [29 Mar 2014]
Fix another edge case with seeking getting stuck on a frame
The day after 0.8.1.2 was released, a failing file was sent to me
Sample .mts file had packets needed for a pts time scattered throughout the file
It's fixed now, but a file like that can be slow to decode as it was made so you have to read almost the entire file to get 1 frame of video
Fix doesn't affect speed of decoding files that aren't like that (I think it's a test sample made to try and break decoders)
On the other hand FFMpeg plugin is more robust at decoding bizarre .mts files

v0.8.1.2 [28 Mar 2014]
Fixed some formats with large keyframe intervals or weird pts/dts had trouble seeking and would only show keyframes after the first seek
Extra seeking further back to find a valid keyframe is done now if an initial seek fails
Some seeking optimizations
Updated FFMpeg dlls to 27/03/2014 - v2.2.0 isn't on zeranoes site, so these are the first dlls after v2.2.0

v0.8.1.1 [20 Mar 2014]
After a night sleep, thought of a better way to get the very end frames of the source file 
Sorry about the quick release but it's better now and didn't want to hold it just because it was so soon

v0.8.1.0 [19 Mar 2014]
Problem that caused incomplete video frames to be returned to virtualdub (garbled moving blocks) especially after seeking
Problem with some formats (eg mkv) when seeking back to the start of the file
Problem with the wrong video frame being shown (repeatedly) after turning audio display off and back on
Better handling of video/audio at the end frames of the file
Added installer c/o raffriff42

v0.8.0.9 [3 Mar 2014]
Bugfix for audio downsampling mistake I introduced in 0.8.0.7
Replaced download with one that doesn't require AVX cpu - sorry about that

v0.8.0.8 [27 Feb 2014]
Updated ffdlls to 2.1.4

v0.8.0.7 [23 Jan 2014]
Added option, default video decoding format for unusual formats
(There are many formats that virtualdub doesn't handle, now you can choose how they are decoded)
Updated ffdlls to 2.1.3

0.8.0.6
Fixed crash from FFDshow dlls logging
Added more native virtualdub video formats, so less are converted to rgb
-> yuv 410p, 422p, 444p, nv12, nv21
Changed default format to rgb32 from rgb24

0.8.0.5
Added option for FFMPeg autodetecting 709 colorspace - if that info is in the source file
Added option for setting 709 colorspace for source file height>=720
Use the ask for extended options checkbox on opening a video to set them
Disabled by default


0.8.0.4
Fixed so registry works on XP
Includes required ffmpeg dlls

0.8.0.3
Settings saved in registry

0.8.0.2 (a)
The "Adjust Pixel Aspect Ratio" option is disabled by default
0.8.0.2a plugin

0.8.0.2
- Implemented full support for multichannel.
- Fixed a memory leak.
- When opening a file with the plugin, if you select "Ask for extened options after this dialog", you get the following options for audio
-- Downmix Audio - if selected, downmixes multichannel to stereo (off by default).
-- Limit Audio To 16-bit - converts 32-bit audio source to 16-bit samples (off by default).

Note that virtualdub has some limitations when dealing with multichannel or 32-bit source audio:
- Some audio compression codecs do not accept multichannel or 32-bit samples as input.
- Virtualdub menu Audio->Conversion does not know how to mix multichannel down to stereo, or convert 32-bit audio to 16-bit.
- Therefore, above options in "Open video file" were implemented to convert to stereo or 16-bit if necessary.
0.8.0.2 plugin
0.8.0.2 source

0.8.0.0
- Upgraded to current versions of ffmpeg - avcodec-55, avformat-55, avutils-52, swscale-2.
- Minimized usage of deprecated features in ffmpeg.
- Added support for planar audio formats.
- Added support for 32-bit audio samples.
- There's tentative support for multichannel, but Virtualdub hangs when fed multichannel audio by an input plugin. So for now, multichannel is down-mixed to stereo as before. See Below.
- Fixed a bug that caused garbled audio and occasional crashes near the end of playback.
- Split the sources to make them more manageable.
- VS2012 project files. 