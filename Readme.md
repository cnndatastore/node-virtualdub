# virtualdub-wrapper #

A simple wrapper around virtualdub for Node.js. Windows-only.

### How to use ###

	var virtualdub = require("./");

	// encode in.mov to out.mp4 with h.264 video, AAC audio, and the YADIF filter
	// overwrite the output file if it already exists (default is true)
	var encoder = virtualdub({
		input: "in.mxf",
		output: "out.avi",
		script: "1920x1080.vcf",
		complete: function(error, complete) {
			// won't actually trigger an error, even if there is an error
			console.log("ENCODING COMPLETE", error, complete);
		}
	});

	// start the encoding
	encoder.start();

	// stop the encoder after 20 seconds
	setTimeout(function() {
		encoder.stop();
	}, 20000);